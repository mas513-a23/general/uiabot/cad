# CAD

CAD files based on UiAbot used in MAS514/513 projects A2022. 

Unszip the CAD.zip folder and open UiaBotAssemblyWoutuArm.SLDASM in Solidworks to see the assembly. 

The Jetson Nano is different now and needs to be upgraded in the CAD model. 

Placement of Jetson Nano and RpLiDAR is random.

Sidepanels shortside and frontside are not printed and mounted on the UiAbot. Feel free to do that.
